package main

import (
	"fmt"
	"time"

	"github.com/charmbracelet/bubbles/table"
	tea "github.com/charmbracelet/bubbletea"
	"github.com/charmbracelet/lipgloss"
	"github.com/cli/go-gh/v2/pkg/api"
)

var sizes = []string{"B", "kB", "MB", "GB", "TB", "PB", "EB"}

func humanReadableSize(s float64, base float64) string {
	unitsLimit := len(sizes)
	i := 0
	for s >= base && i < unitsLimit {
		s = s / base
		i++
	}

	f := "%.0f %s"
	if i > 1 {
		f = "%.2f %s"
	}

	return fmt.Sprintf(f, s, sizes[i])
}

var baseStyle = lipgloss.NewStyle().
	BorderStyle(lipgloss.NormalBorder()).
	BorderForeground(lipgloss.Color("240"))

type model struct {
	table table.Model
}

func (m model) Init() tea.Cmd { return nil }

func (m model) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	var cmd tea.Cmd
	switch msg := msg.(type) {
	case tea.KeyMsg:
		switch msg.String() {
		case "esc":
			if m.table.Focused() {
				m.table.Blur()
			} else {
				m.table.Focus()
			}
		case "q", "ctrl+c":
			return m, tea.Quit
		case "enter":
			return m, tea.Batch(
				tea.Printf("Download URL: curl -LO %s", m.table.SelectedRow()[2]),
			)
		}
	}
	m.table, cmd = m.table.Update(msg)
	return m, cmd
}

func (m model) View() string {
	return baseStyle.Render(m.table.View()) + "\n"
}

type Release []struct {
	URL             string    `json:"url,omitempty"`
	AssetsURL       string    `json:"assets_url,omitempty"`
	UploadURL       string    `json:"upload_url,omitempty"`
	HTMLURL         string    `json:"html_url,omitempty"`
	ID              int       `json:"id,omitempty"`
	NodeID          string    `json:"node_id,omitempty"`
	TagName         string    `json:"tag_name,omitempty"`
	TargetCommitish string    `json:"target_commitish,omitempty"`
	Name            string    `json:"name,omitempty"`
	Draft           bool      `json:"draft,omitempty"`
	Prerelease      bool      `json:"prerelease,omitempty"`
	CreatedAt       time.Time `json:"created_at,omitempty"`
	PublishedAt     time.Time `json:"published_at,omitempty"`
	Assets          []Assets  `json:"assets,omitempty"`
	TarballURL      string    `json:"tarball_url,omitempty"`
	ZipballURL      string    `json:"zipball_url,omitempty"`
	Body            string    `json:"body,omitempty"`
}

type Assets struct {
	URL                string    `json:"url,omitempty"`
	ID                 int       `json:"id,omitempty"`
	NodeID             string    `json:"node_id,omitempty"`
	Name               string    `json:"name,omitempty"`
	Label              string    `json:"label,omitempty"`
	ContentType        string    `json:"content_type,omitempty"`
	State              string    `json:"state,omitempty"`
	Size               int       `json:"size,omitempty"`
	DownloadCount      int       `json:"download_count,omitempty"`
	CreatedAt          time.Time `json:"created_at,omitempty"`
	UpdatedAt          time.Time `json:"updated_at,omitempty"`
	BrowserDownloadURL string    `json:"browser_download_url,omitempty"`
}

func main() {
	client, err := api.DefaultRESTClient()
	if err != nil {
		fmt.Println(err)
		return
	}

	response := Release{}
	err = client.Get("repos/kubernetes-sigs/cluster-api/releases", &response)
	if err != nil {
		fmt.Println(err)
		return
	}

	columns := []table.Column{
		{Title: "Asset Name", Width: 50},
		{Title: "Size", Width: 20},
		{Title: "Download URL", Width: 20},
	}

	var rows []table.Row

	for _, rls := range response {
		for _, assetname := range rls.Assets {
			rows = append(rows, table.Row{assetname.Name, humanReadableSize(float64(assetname.Size), 1024.0), assetname.BrowserDownloadURL})
		}
	}

	t := table.New(
		table.WithColumns(columns),
		table.WithRows(rows),
		table.WithFocused(true),
		// table.WithHeight(10),
	)

	s := table.DefaultStyles()
	s.Header = s.Header.
		BorderStyle(lipgloss.NormalBorder()).
		BorderForeground(lipgloss.Color("240")).
		BorderBottom(true).
		Bold(false)
	s.Selected = s.Selected.
		Foreground(lipgloss.Color("229")).
		Background(lipgloss.Color("57")).
		Bold(false)
	t.SetStyles(s)

	m := model{t}
	if _, err := tea.NewProgram(m).Run(); err != nil {
		fmt.Println("Error running program:", err)
	}
}
